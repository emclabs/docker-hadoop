*** Settings ***
| Documentation   |  A test suite containing tests related to TIM ITALY

| # http://robotframework.org/SSHLibrary/latest/SSHLibrary.html
| Library    |      SSHLibrary
| # http://robotframework.org/robotframework/latest/libraries/BuiltIn.html
| Library    |      BuiltIn

| Suite Setup    |      Open Connection And Log In
| Suite Teardown |      Close All Connections

*** Variable ***
| ${GIT_NAME}      | "tim-it-inge-flussi-campaign"
| ${GIT_CLONE}     | "https://devopsdellemc:D3lL.3mC@bitbucket.org/emcbrasil/${GIT_NAME}.git"

| ${HOST}     |      hive
| ${USERNAME} |      hive
| ${PASSWORD} |      hive

| ${DATA_TEMPLATE} | "/hive_test_sh.tar.gz"
| ${PATH_IN}       | /home/hive/${GIT_NAME}/Backend/src/main/sh
| ${PATH_CARGA}    | /var/lib/packages/massateste
| ${RUN_CARGA}     | /${GIT_NAME}/Backend/src/main/sh/run_load_test.sh
| ${PATH_OUT}      | xxxxx
| ${PATH_FAIL}     | xxxxx

*** Test Cases ***
| # Clone
| Clone files from BitBicket
|  |  Execute Command  |  git clone ${GIT_CLONE}

| # Move
| Move data files to input folder
|  |  Execute Command  |  tar -C ${PATH_IN} -zxvf ${DATA_TEMPLATE}

| # Move Carga Teste
| Move Carga Teste
|  |  Execute Command  |  mkdir ${PATH_IN}/../landing
|  |  Execute Command  |  mv ${PATH_CARGA}/*.txt.gz ${PATH_IN}/../landing

| #3 Run
| Run CARGA
|  |  Execute Command  |  ${RUN_CARGA}

#| #3.1 Assert not failed
#| Assert if file exists in folder after success execution
#|  |  File Should Not Exist | ${PATH_FAIL}

#| #4 Assert
#| Assert if file exists in folder after success execution
#|  |  File Should Exist | ${PATH_OUT}

| #5 Assert Content with count
|  | #TearDown
|  | Disconnect From Database

*** Keywords ***
| Open Connection And Log In
|  | Open Connection | ${HOST}
|  | Login           | ${USERNAME} | ${PASSWORD}
#ssh -oStrictHostKeyChecking=no hive@hive